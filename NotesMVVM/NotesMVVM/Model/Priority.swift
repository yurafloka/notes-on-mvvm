//
//  Priority.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation
import UIKit
enum Priority: String {
  case low
  case medium
  case high

  var color: UIColor {
    switch self {
    case .high: return UIColor.red
    case .medium: return UIColor.orange
    case .low: return UIColor.green
    }
  }
}
