//
//  TaskRepository.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

class TasksRepository {

  private init() {}

  static let shared = TasksRepository()

  var tasks: [Task] = []

  func addTask(_ task: Task) {
    tasks.append(task)
  }

  func getTaskById(_ id: String) -> Task? {
    guard let index = tasks.index(where: { $0.id == id }) else { return nil }
    return tasks[index]
  }

  func getAllTasks() -> [Task] {
    return self.tasks
  }

  func generateIntialData() {
    let task = Task(name: "Go home", description: "Clean the room", image: "default", priority: Priority.high)
    addTask(task)
  }
}
