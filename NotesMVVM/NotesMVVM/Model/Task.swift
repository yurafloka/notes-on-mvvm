//
//  Task.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation
struct Task {
  let id: String = UUID().uuidString
  let name: String
  let description:String
  let image:String
  let priority: Priority

}
