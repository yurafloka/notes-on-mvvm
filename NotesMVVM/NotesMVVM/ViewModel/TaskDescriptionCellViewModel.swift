//
//  TaskDescriptionCellViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 12.06.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

struct TaskDescriptionCellViewModel {
  let category: String
  let content: String
}
