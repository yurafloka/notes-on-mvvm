//
//  TasksCellViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 11.06.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

struct TasksCellViewModel {
  let id:String
  let name:String
  let description:String
  let image:String
  let priority:Priority

  init(_ task: Task) {
    id = task.id
    name = task.name
    description = task.description
    image = task.image
    priority = Priority(rawValue: task.priority.rawValue)!
  }
}
