//
//  LoginViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/12/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

struct LoginViewModel {

  var showAlert: ((AlertViewModel) -> Void)?

  var showTasksList: ((TasksViewModel) -> Void)?

  func login(withName name:String,withEmail email:String) {
    if name.isEmpty || email.isEmpty  {
      showAlert?(AlertViewModel(title: "Sorry", message: "Bad credentials"))
    } else {
      UserDefaults.standard.set(true, forKey: "UserLoggedIn")
      let taskViewModel = TasksViewModel()
      showTasksList?(taskViewModel)
    }
  }

  func checkSession() {
    if UserDefaults.standard.bool(forKey: "UserLoggedIn") == true {
      let taskViewModel = TasksViewModel()
      showTasksList?(taskViewModel)
    }
  }
}


