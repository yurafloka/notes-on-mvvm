//
//  TasksModelView.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 11.06.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

struct TasksViewModel {

  var reloadingData: (() -> Void)?

  var showTaskDetail: ((TaskDescriptionViewModel) -> Void)?

  var numberOfCells: Int {
    return cellViewModels.count
  }

  private var cellViewModels: [TasksCellViewModel] = []

  private var tasks: [Task] = []

  func logout() {
    UserDefaults.standard.set(false, forKey: "UserLoggedIn")
  }

  mutating func fetchingTasks() {
    tasks = TasksRepository.shared.getAllTasks()

    self.cellViewModels = tasks.map { TasksCellViewModel($0) }
    self.reloadingData?()
  }
  
  func selectTask(atIndex index: Int) {
    let taskDescription = TaskDescriptionViewModel(task: tasks[index])
    self.showTaskDetail?(taskDescription)
  }

  func getCellViewModel(at index: Int) -> TasksCellViewModel {
    return cellViewModels[index]
  }
}
