//
//  AddTaskViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 12.06.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation
struct AddTaskViewModel {

  var showAlert: ((AlertViewModel) -> Void)?

  var showTasksList: ((AddTaskViewModel) -> Void)?

  func savingTask(_ task: Task) {
    TasksRepository.shared.addTask(task)
  }

  func saveTask(withName name:String,withDescription description:String,withPriority priority: String){
    if name.isEmpty || description.isEmpty || priority.isEmpty {
      showAlert?(AlertViewModel(title: "Sorry", message: "Bad credentials"))
    } else {
      let taskForSave = Task(name: name, description: description, image: "default", priority: Priority(rawValue: priority)!)
      TasksRepository.shared.addTask(taskForSave)
      let addTaskViewModel = AddTaskViewModel()
      showTasksList?(addTaskViewModel)
    }
  }
}
