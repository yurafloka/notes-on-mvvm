//
//  AlertViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/12/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation
import UIKit

struct AlertViewModel {
  let title: String
  let message: String
}
