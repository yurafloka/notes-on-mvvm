//
//  TaskDescriptionViewModel.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 12.06.18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import Foundation

struct TaskDescriptionViewModel {

  private let task: Task
  private(set) var taskDescriptionCells: [TaskDescriptionCellViewModel] = []

  init(task:Task) {
    self.task = task
    configureCells()
  }

  private mutating func configureCells() {
    taskDescriptionCells.append(TaskDescriptionCellViewModel(category:"Name",content: task.name))
    taskDescriptionCells.append(TaskDescriptionCellViewModel(category: "Description", content: task.description))
    taskDescriptionCells.append(TaskDescriptionCellViewModel(category: "Image", content: task.image))
    taskDescriptionCells.append(TaskDescriptionCellViewModel(category: "Priority", content: task.priority.rawValue))
  }

}
