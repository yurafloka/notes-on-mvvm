//
//  AddTaskVC.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class AddTaskVC: UIViewController {
  
  var viewModel = AddTaskViewModel()

  private let router = Router()
  
  @IBOutlet weak private var taskImage: UIImageView!
  
  @IBOutlet weak private var taskName: UITextField!
  
  @IBOutlet weak private var taskDescription: UITextField!
  
  @IBOutlet weak private var taskPriority: UITextField!
  
  @IBAction private func didPressSaveButton(_ sender: UIButton) {
    viewModel.saveTask(withName: taskName.text!, withDescription: taskDescription.text!, withPriority: taskPriority.text!)
  }

  override func viewDidLoad() {
    observeViewModel()
    super.viewDidLoad()
  }

  private func observeViewModel() {
    viewModel.showAlert = { [weak self] alertViewModel in
      self?.showAlert(withViewModel: alertViewModel)
    }
    viewModel.showTasksList = { [weak self] taskViewModel in
      self?.navigateToTaskVC(withViewModel: taskViewModel)
    }
  }

  private func navigateToTaskVC(withViewModel viewModel: AddTaskViewModel) {
    let tasksVC = self.storyboard?.instantiateViewController(withIdentifier: "TasksVC")
    self.navigationController?.pushViewController(tasksVC!, animated: true)
  }

  private func showAlert(withViewModel viewModel: AlertViewModel ) {
    router.showAlert(viewModel, inViewController: self)
  }
}

