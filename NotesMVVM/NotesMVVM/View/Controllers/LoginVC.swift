//
//  LoginVC.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

  var viewModel = LoginViewModel()

  private let router = Router()
  
  @IBOutlet weak private var nameTextField: UITextField!
  
  @IBOutlet weak private var emailTextField: UITextField!
  
  @IBAction private func didPressLoginButton(_ sender: UIButton) {
    viewModel.login(withName: nameTextField.text!,withEmail: emailTextField.text!)
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    observeViewModel()
    viewModel.checkSession()
    TasksRepository.shared.generateIntialData()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    nameTextField.text = ""
    emailTextField.text = ""
  }

  private func observeViewModel() {
    viewModel.showTasksList = { [weak self] taskViewModel in
      self?.navigateToTaskVC(withViewModel: taskViewModel)
    }
    viewModel.showAlert = { [weak self] alertViewModel in
      self?.showAlert(withViewModel: alertViewModel)
    }
  }

  private func navigateToTaskVC(withViewModel viewModel: TasksViewModel) {
    let tasksVC = self.storyboard?.instantiateViewController(withIdentifier: "TasksVC")
    self.navigationController?.pushViewController(tasksVC!, animated: false)
  }


  private func showAlert(withViewModel viewModel: AlertViewModel ) {
    router.showAlert(viewModel, inViewController: self)
  }
}
