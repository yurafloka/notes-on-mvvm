//
//  AlertViewController.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/12/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class AlertViewController: UIAlertController {

  var viewModel: AlertViewModel?

  override func viewDidLoad() {
    super.viewDidLoad()
    self.message = viewModel?.message
    self.title = viewModel?.title
    addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
  }
}
