//
//  TasksVC.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class TasksVC: UIViewController {

  var viewModel = TasksViewModel()
  
  @IBOutlet weak private var tasksTableView: UITableView!

  @IBAction private func didPressLogoutButton(_ sender: UIBarButtonItem) {
    viewModel.logout()
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction private func didPressTaskButton(_ sender: UIBarButtonItem) {
    let addTaskVC = self.storyboard?.instantiateViewController(withIdentifier: "AddTaskVC")
    self.navigationController?.pushViewController(addTaskVC!, animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
    observeViewModel()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    observeViewModel()
  }

  func setupViews() {
    tasksTableView.estimatedRowHeight = 150
    tasksTableView.rowHeight = UITableViewAutomaticDimension
  }

  func observeViewModel() {
    viewModel.fetchingTasks()
    viewModel.reloadingData = { [weak self] in
      DispatchQueue.main.async {
        self?.tasksTableView.reloadData()
      }
    }
    viewModel.showTaskDetail = { [weak self] taskDescriptionViewModel in
      self?.showTaskDescription(withViewModel: taskDescriptionViewModel)
    }
  }

  private func showTaskDescription(withViewModel viewModel: TaskDescriptionViewModel) {
    let taskDescriptionVC = UIStoryboard(name: "Main", bundle: nil)
      .instantiateViewController(withIdentifier: "TaskDescriptionVC") as! TaskDescriptionVC
    taskDescriptionVC.viewModel = viewModel
    self.navigationController?.pushViewController(taskDescriptionVC, animated: true)
  }
}

extension TasksVC: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TasksTableViewCell
    cell.viewModel = viewModel.getCellViewModel(at: indexPath.row)
    return cell
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    viewModel.selectTask(atIndex: indexPath.row)
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.numberOfCells
  }
}
