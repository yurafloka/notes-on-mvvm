//
//  TaskDescriptionVC.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class TaskDescriptionVC: UIViewController {

  var viewModel: TaskDescriptionViewModel?

  @IBOutlet weak private var taskDescriptionTableView: UITableView!

  override func viewDidLoad() {
    super.viewDidLoad()
    setupViews()
  }

  private func setupViews() {
    taskDescriptionTableView.estimatedRowHeight = 150
    taskDescriptionTableView.rowHeight = UITableViewAutomaticDimension
  }
}

extension TaskDescriptionVC: UITableViewDelegate, UITableViewDataSource {

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "TaskDescriptionCell", for: indexPath) as! TaskDescriptionTableViewCell
    cell.viewModel = viewModel?.taskDescriptionCells[indexPath.row]
    return cell
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel?.taskDescriptionCells.count ?? 0
  }
}
