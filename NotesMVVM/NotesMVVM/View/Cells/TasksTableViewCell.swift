//
//  TasksVCCell.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class TasksTableViewCell: UITableViewCell {

  var viewModel: TasksCellViewModel? {
    didSet {
      setup()
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    setup()
  }

  private func setup() {
    guard let viewModel = viewModel else { return }
    taskNameLabel.text = viewModel.name
    taskView.backgroundColor = viewModel.priority.color
  }
  
  @IBOutlet weak private var taskView: UIView!
  
  @IBOutlet weak private var taskNameLabel: UILabel!

}
