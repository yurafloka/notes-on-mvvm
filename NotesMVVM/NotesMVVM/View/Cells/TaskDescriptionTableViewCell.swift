//
//  TaskDescriptionCell.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

class TaskDescriptionTableViewCell: UITableViewCell {

  var viewModel: TaskDescriptionCellViewModel! {
    didSet {
      setup()
    }
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    setup()
  }
  
  private func setup() {
    contentLabel.text = viewModel.content
    infoLabel.text = viewModel.category
  }

  @IBOutlet weak private var contentLabel: UILabel!
  
  @IBOutlet weak private var infoLabel: UILabel!
  
}
