//
//  AppDelegate.swift
//  NotesMVVM
//
//  Created by Yurii Tsymbala on 6/11/18.
//  Copyright © 2018 Yurii Tsymbala. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    return true
  }
}

